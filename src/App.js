/* import logo from './logo.svg';*/
import { useRef } from 'react';
import './App.css';
import Devices from './components/devices';
import Terminal from './components/terminal';

function App() {
	const container = useRef(null);
  return (
    <div className="container" ref={container}>
		<Devices />
		<Terminal parent={container} />
    </div>
  );
}

export default App;
