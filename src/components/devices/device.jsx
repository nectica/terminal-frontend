function Device(props) {
	const getStatusColor = (status) => {
		let classCss = [];
		switch (status*1) {
			case 1 :
				classCss.push('success-color');
				break;
			case 0 :
				classCss.push('error-color');
				break;
			case 'warning':
				classCss.push('warning-color');
				break;
			default:
				break;
		}

		return classCss.join(' ');
	}
	const getTypeEvent = (event) => {
		switch (event) {
		case 'manAlive':
			return '';
			break;
		case 'novedades/imagen':
			return '';
			break;
		case 'todoOk':
			return '';
			break;
		case 'ronda':
			return '';
			break;
		case 'novedades':
			return '';
			break;
		case 'novedades/audio':
			return '';
			break;
		case 'OnLine':
			return '';
			break;
		default:
			return '';
			break;
		}
	}
	const fetchObjetivo = (data) => {

		props.modal((prevstate) => {
			return ({ show: true, typeData: 'objectivo', data: {...data} });
		});
	}

	return (
		<li>
			<span className="counter-device"><a href={'id/'+props.data.id}> Nº {props.data.id}</a></span>
			<span className="details-device"><a onClick={() => fetchObjetivo(props.data)} /* href={'objectivo/' +props.data.id_objetivo} */ >{props.data.nombre_objetivo} - {props.data.tipo_evento } </a> </span>
			<span className="name-device">{props.data.nombre_operador}</span>
			<span className="status-device"> <span className={`circle-status ${getStatusColor(props.data.status)}`} ></span></span>
		</li>
	);
}
 export default Device;
