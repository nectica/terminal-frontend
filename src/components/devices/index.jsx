import  { Fragment, useEffect, useState } from 'react';
import useInterval from '../../hooks/UseSetInterval';
import Device from './device';
import Modal from '../modal/';

function Devices(props) {
	const [devices, setDevices] = useState({original: [], copy: [], last: 0});
	const [time, setTime] = useState(10000);
    const [paginate, setPaginate] = useState(10);
	const [modal, setModal ] = useState({show: false, typeData: '', data: []});

	useEffect (()=>{

	},[]);
	useInterval(() => {
        const url = `http://localhost:8888/CRUD/Devices.php?id=${devices.last}`;
        const request = fetch(url)
			.then( response => response.json() )
			.then(json => {
				console.log('json', json);
				if (json.status === "success" && json.data.length > 0) {
					setDevices((prevState) => {
						const unshift = [ ...json.data, ...prevState.original];
						return {original: unshift, copy: unshift, last: json.data[0].id };
					} );
				}
			});

	}, time);
	const addPaginate = () =>{
		if(devices.copy.length !== 0) {
			setPaginate((prevState) =>{
				const diff = prevState - devices.copy.length;
				if ( diff > 0 && diff < 10   ) {
					return devices.copy.length ;
				}
				return prevState+10;
			} );
		}
	}

	return (
		<Fragment>
			<ul className="device-list">
				{devices.copy.slice(0, paginate).map((device,index) => <Device key={'device-'+device.id} data={device}  number={ device.id } modal={setModal} /> ) }
				{ devices.copy.length > 10 ?  <li className="mostrar-mas-section"> <button className="mostrar-mas-button" onClick={addPaginate}>Mostra mas</button></li> : ''}
			</ul>
			{ modal.show ? <Modal close={setModal} state={modal}  /> : '' }
		</Fragment>
	);

}
export default Devices;
