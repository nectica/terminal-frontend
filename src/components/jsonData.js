const today = new Date() ;
const now = today.getDay()+'/'+(today.getMonth() +1 )+'/'+today.getFullYear()+' '+today.getHours()+':'+today.getMinutes()+':'+today.getSeconds()+':'+today.getMilliseconds();
const jsonExample = [
	{id:1, status: 'warning', event: 'ronda vuelta de forma recta.', person: 'Jose Hernandez', created_at: now },
	{id: 2, status: 'success', event: 'test comprobando activo', person: 'Pedro Hernandez', created_at: now },
	{id: 3, status: 'danger', event: 'test no respondio.', person: 'Jose Martinez', created_at: now },
	{id: 4, status: 'primary', event: 'esperando terminar ronda', person: 'Ivan fernandez', created_at: now },
	{id: 5, status: 'warning', event: 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Iste, ab! Explicabo soluta fugit cumque ratione veritatis facere modi ab numquam deserunt, assumenda rerum obcaecati totam sed tenetur et ullam ipsam?Lorem ipsum dolor sit amet consectetur adipisicing elit. Iste, ab! Explicabo soluta fugit cumque ratione veritatis facere modi ab numquam deserunt, assumenda rerum obcaecati totam sed tenetur et ullam ipsam?Lorem ipsum dolor sit amet consectetur adipisicing elit. Iste, ab! Explicabo soluta fugit cumque ratione veritatis facere modi ab numquam deserunt, assumenda rerum obcaecati totam sed tenetur et ullam ipsam?', person: 'Jose Hernandez', created_at: now },

];
export default jsonExample;
