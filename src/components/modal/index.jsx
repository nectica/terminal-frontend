export default function Modal(props) {
	const close = () =>{
		props.close((prevState)=>{
			return ({...prevState, show: false})
		});
	}
	return (
		<div className="modal">
			<div className="modal-content">
				<div className="modal-content-buttons">
					<button className='modal-close-button' onClick={close}>Cerrar</button>
				</div>
				<div className="modal-content-text">
					<p>id: { props.state.data.id }</p>
				</div>
			</div>
		</div>
	);
}
