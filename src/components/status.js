const status = [
	{nombre: 'warning', value:2 },
	{nombre: 'success', value:1 },
	{nombre: 'danger', value:0 },
	{nombre: 'primary', value:3 },
];
export default status;
