import { useEffect, useState } from "react";


function ItemConsole(props) {

	const [classListContainer, setClassListContainer] = useState(['terminal-log-item-container']);
	const [classList, setClassList] = useState(['console-log-item']);
	useEffect(()=> {
		addClass();
	},[props.added]);
	const addClass = () => {
		if (props.added) {
			addNewClassToStateContainer('new');
		}else{
			addNewClassToStateContainer('OLD');
		}
		switch (props.data.status * 1) {
			case 2:
				addNewClassToState('text-warning');
				break;
			case 1:
				addNewClassToState('text-success');
				break;
			case 0:
				addNewClassToState('text-danger');
				break;
			case 'primary':
				addNewClassToState('text-primary');
			default:
				addNewClassToState('text-success');
				break;
		}
	}
	const addNewClassToStateContainer = (className) =>{
		setClassListContainer((prevState)=> {
			if(className === 'OLD'){
				return [ ...prevState.filter( item => item !== 'new') ]
			}
			return [className, ...prevState]
		});
	}
	const addNewClassToState = (className) =>{
		setClassList((prevState)=> {

			return [className, ...prevState]
		});
	}
	return (
		<div className={classListContainer.join(' ')}>
			<a href="#" className= { classList.join(' ') } > {props.data.id }- {props.data.nombre_operador} - {props.data.nombre_objetivo} - {props.data.tipo_evento}</a>
			<span>{props.data.fecha} </span>
		</div>
	);
}

export default ItemConsole;
