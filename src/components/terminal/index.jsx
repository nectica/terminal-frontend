import { useEffect, useRef, useState } from 'react';
import ItemTerminal from './ItemTerminal';
import status from '../status';
import useInterval from '../../hooks/UseSetInterval';
function Terminal(props) {
    const ref = useRef(null);
    const buttonRef = useRef(null);
    const [terminalFullSize, setTerminalFullSize] = useState(false);
	const [time, setTime] = useState(10000);
	const [paginate, setPaginate] = useState(10);
    const [classList, setClassLit] = useState(['terminal']);
    const [bodyAllItems, setBodyAllItems] = useState({ original: [], copy: [], last: 0, added:[] });

    useEffect(() => {

    }, []);
    const handleClick = () => {
		props.parent.current.classList.toggle('full-size-terminal')
		setTerminalFullSize( (prevState) => !prevState );
	}

	useInterval( () => {
		const url = `http://localhost:8888/CRUD/Eventos.php?id=${bodyAllItems.last}`;

		const request = fetch(url);
		request.then((response) =>  response.json() )
			.then((json) =>{
				if (json.status === "success" && json.data.length > 0) {
					setBodyAllItems(
						function(prevState){
							const unshift = [...json.data, ...prevState.original];
							return {original: unshift, copy: unshift, last: json.data[0].id, added: prevState.original.length > 0 ? json.data.map(item => item.id) : [] };
						});
				}
			} );
		return true;
	}, time);

    const handlePlayButton = (evt) => {
        const text = evt.target.innerText;
        switch (text) {
            case 'Pausa':
                pauseFetch(evt.target)
                break;
            case 'Continuar':
                startFetch(evt.target);
                evt.target.innerText = 'Pausa';
                break;
        }
    }
    const pauseFetch = (button) => {
		setTime(null);
        toggleText(button, 'Continuar');
    }
    const startFetch = (button) => {
		setTime(10000);
        toggleText(button, 'Pausa');
    }
    const toggleText = (button, text) => {
        button.innerText = text;
    }
    const handleSearch = (evt) => {
        pauseFetch(buttonRef.current);
        const copyState = { ...bodyAllItems, copy:bodyAllItems.original.filter((item) => item.event.includes(evt.target.value)) };
        setBodyAllItems(copyState);
    }
    const handleChange = (evt) => {
        pauseFetch(buttonRef.current);
        if (evt.target.value !== "0") {
            const copyState = { ...bodyAllItems, copy: bodyAllItems.original.filter((item) => item.status === evt.target.value) };
            setBodyAllItems(copyState);
        } else {
            const copyState = { ...bodyAllItems, copy: bodyAllItems.original };
            setBodyAllItems(copyState);
        }
	}
	const addPaginate = () =>{
		if(bodyAllItems.copy.length !== 0) {
			setPaginate((prevState) =>{
				const diff =prevState - bodyAllItems.copy.length;
				if ( diff > 0 && diff < 10   ) {
					return bodyAllItems.copy.length ;
				}
				return prevState+10;
			} );
		}
	}

    return (
        <section ref={ref} className={classList.join(' ')}>
            <div className="terminal-header">
                <ul>
                    <li>
						<span className="icon-terminal"></span><input className="searcher" type="text" name="search" onKeyUp={handleSearch} placeholder="Filtrar" />
                    </li>
                    <li>
                        <button ref={buttonRef} onClick={handlePlayButton} name="play">Pausa</button>
                    </li>
                    <li>
                        <select name="status" onChange={handleChange}>
                            <option value={0}  >Todos</option>
                            {status.map((statu, id) => <option key={'status-' + id} value={statu.value}>{statu.nombre}</option>)}
                        </select>
                    </li>
					<li>
						<button onClick={handleClick}>{ !terminalFullSize ? 'Max': 'Min'}</button>
					</li>
                </ul>
            </div>
            <div className="terminal-body">
                {bodyAllItems.copy.slice(0, paginate).map((item, index) => <ItemTerminal added={bodyAllItems.added.includes(item.id)} key={'itemTerminal-' + item.id} data={item} />)}
				{ bodyAllItems.copy.length > 10 ? <ul> <li className="mostrar-mas-section"> <button className="mostrar-mas-button" onClick={addPaginate}>Mostra mas</button></li></ul>   : ''}
            </div>
        </section>);
}
export default Terminal;
